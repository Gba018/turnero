
    function autocompletarCampos() {
        var ci = document.getElementById('ci').value;
      
        if (ci.length === 10) {
          // Realizar la petición Fetch al servidor
          fetch('/obtener_datos_cliente/', {
            method: 'POST',  // Método de la petición (puede ser GET o POST según tu configuración)
            headers: {
              'Content-Type': 'application/json'  // Tipo de contenido de la solicitud
            },
            body: JSON.stringify({ ci: ci })  // Datos a enviar al servidor en formato JSON
          })
          .then(response => response.json())  // Parsear la respuesta del servidor a JSON
          .then(data => {
            // Obtener los datos del cliente desde la respuesta del servidor
            var name = data.name;
            var clientType = data.clientType;
      
            // Llenar automáticamente los campos restantes con los datos obtenidos
            document.getElementById('name').value = name;
            document.getElementById('client-type').value = clientType;
          })
          .catch(error => {
            // Manejar el error de la petición Fetch
            console.log(error);
          });
        }
      }
      
      function autocompletarCampos() {
        // Obtener el valor del tipo de cliente seleccionado
        const clientType = document.getElementById('client-type').value;
      
        // Obtener los campos adicionales y su contenedor
        const camposAdicionales = document.getElementById('campos-adicionales');
        const campoPrioridad = document.getElementById('priority');
      
        // Limpiar los campos adicionales y ocultarlos inicialmente
        camposAdicionales.innerHTML = '';
        camposAdicionales.style.display = 'none';
      
        // Asignar la prioridad basada en el tipo de cliente seleccionado
        let priority = 1; // Prioridad baja para clientes personales
        if (clientType === 'alto') {
          priority = 3; // Prioridad alta para clientes prioritarios
        } else if (clientType === 'medio') {
          priority = 2; // Prioridad media para clientes corporativos
        }
      
        // Si la prioridad es distinta de 1 (personal), mostrar el campo de prioridad
        if (priority !== 1) {
          const labelPrioridad = document.createElement('label');
          labelPrioridad.textContent = 'Prioridad:';
          const inputPrioridad = document.createElement('input');
          inputPrioridad.type = 'text';
          inputPrioridad.name = 'priority';
          inputPrioridad.value = priority;
          inputPrioridad.readOnly = true;
      
          camposAdicionales.appendChild(labelPrioridad);
          camposAdicionales.appendChild(inputPrioridad);
          camposAdicionales.style.display = 'block';
        }
      
        // Asignar la prioridad al campo oculto 'priority'
        campoPrioridad.value = priority;
      }
      