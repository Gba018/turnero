from django.contrib import admin
from .models import Cliente, Turno

admin.site.register(Cliente)
admin.site.register(Turno)

