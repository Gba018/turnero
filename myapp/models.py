from django.db import models


class Cliente(models.Model):
    ci = models.CharField(max_length=20)
    name = models.CharField(max_length=100)
    client_type = models.CharField(max_length=20)

    def __str__(self):
        return self.name

class Turno(models.Model):
    cliente = models.ForeignKey(Cliente, on_delete=models.CASCADE)
    hora_llegada = models.DateTimeField(auto_now_add=True)
    prioridad = models.IntegerField()

    def __str__(self):
        return f"Turno #{self.id} - {self.cliente.name}"
