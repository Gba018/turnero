from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.http import JsonResponse
from .models import Cliente, Turno
from queue import PriorityQueue
from datetime import datetime
from django.contrib.auth.decorators import login_required


cola_prioridad = PriorityQueue()
cola_turnos = []
turnos_queue = PriorityQueue()

class ClienteTurno:
    def __init__(self, ci, name, client_type, hora_llegada, priority):
        self.ci = ci
        self.name = name
        self.client_type = client_type
        self.hora_llegada = hora_llegada
        self.priority = priority  # Almacena priority como atributo

    def __lt__(self, other):
        return self.priority < other.priority


def index(request):
    return render(request, "index.html")

@login_required
def inicio(request):
    if request.method == 'POST':
        name = request.POST.get('name')
        ci = request.POST.get('ci')
        client_type = request.POST.get('clientType')
        
        # Asignar prioridad basada en el tipo de cliente
        if client_type == 'alto':
            priority = 3  # Prioridad alta para clientes prioritarios
        elif client_type == 'medio':
            priority = 2  # Prioridad media para clientes corporativos
        else:
            priority = 1  # Prioridad baja para clientes personales
        
        hora_llegada = datetime.now()
        cliente = ClienteTurno(ci, name, client_type, hora_llegada, priority)
        cola_turnos.append(cliente)

        # Redirigir a la página de turnos después del registro exitoso
        return redirect('turnos')

    # Si la solicitud no es POST, renderizar la página de inicio
    return render(request, "inicio.html")

@login_required
def turnos(request):
    turnos = sorted(cola_turnos, key=lambda x: (x.priority, x.hora_llegada))
    return render(request, "turnos.html", {'turnos': turnos})

@login_required
def h_turnos(request):
    return render(request, "h_turnos.html")

def inicio_sesion(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            # Redirige a la página deseada después del inicio de sesión exitoso
            return redirect('inicio')
        else:
            # El usuario no está autenticado, puedes mostrar un mensaje de error
            return render(request, 'login.html', {'error': 'Credenciales inválidas'})
    else:
        return render(request, 'login.html')

def cerrar_sesion(request):
    logout(request)
    # Redirige a la página deseada después del cierre de sesión exitoso
    return redirect('inicio')

def obtener_datos_cliente(request):
    if request.method == 'POST':
        ci = request.POST.get('ci')
        try:
            cliente = Cliente.objects.get(ci=ci)
            # Obtén los datos del cliente que deseas devolver
            data = {
                'name': cliente.name,
                'clientType': cliente.client_type
            }
            return JsonResponse(data)
        except Cliente.DoesNotExist:
            # Si no se encuentra el cliente, puedes devolver un mensaje de error
            data = {
                'error': 'Cliente no encontrado'
            }
            return JsonResponse(data, status=404)
