from django.urls import path
from django.contrib.auth import views as auth_views
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('inicio/', views.inicio, name='inicio'),
    path('turnos/', views.turnos, name='turnos'),
    path('h_turnos/', views.h_turnos, name='h_turnos'),
    path('login/', views.inicio_sesion, name='login'),
    path('logout/', views.cerrar_sesion, name='logout'),
    path('obtener_datos_cliente/', views.obtener_datos_cliente, name='obtener_datos_cliente'),
    path('accounts/login/', auth_views.LoginView.as_view(), name='login'),
]
