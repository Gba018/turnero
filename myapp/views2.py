from django.shortcuts import render, redirect
from .models import Cliente, Turno
from queue import PriorityQueue
from django.contrib.auth import authenticate, login, logout
from django.views.decorators.csrf import csrf_protect
from django.http import JsonResponse
from queue import PriorityQueue
from heapdict import heapdict
from datetime import datetime




def h_turnos(request):
    return render(request, "h_turnos.html")

def index(request):
    return render(request, "index.html")
 
@csrf_protect
def inicio(request):
    if request.method == 'POST':
        ci = request.POST.get('ci')
        name = request.POST.get('name')
        client_type = request.POST.get('client-type')

        if Cliente.objects.filter(ci=ci).exists():
            # El cliente ya existe, actualiza sus datos
            cliente = Cliente.objects.get(ci=ci)
            cliente.name = name
            cliente.client_type = client_type
            cliente.save()
        else:
            # El cliente no existe, crea uno nuevo
            cliente = Cliente.objects.create(ci=ci, name=name, client_type=client_type)

        # Resto de la lógica para asignar prioridad y turno
        prioridad = calcular_prioridad(cliente)
        # Agregamos el cliente a la cola de prioridad
        cola_prioridad[cliente.pk] = prioridad
        # Agregamos el cliente a la cola de turnos
        cola_turnos.put((prioridad, cliente.pk))


        return redirect('turnos')  # Redirige a la página de turnos

    return render(request, "inicio.html")

cola_prioridad = PriorityQueue()

cola_turnos = PriorityQueue()

hora_llegada_dict = heapdict()

def calcular_prioridad(cliente):
    # Asignamos la prioridad según el tipo de cliente
    if cliente.client_type == 'prioritario':
        prioridad = 1  # Alta prioridad
    elif cliente.client_type == 'corporativo':
        prioridad = 2  # Mediana prioridad
    else:
        prioridad = 3  # Baja prioridad

    # Obtenemos la hora actual
    hora_actual = datetime.now()

    # Verificamos si el cliente ya está en la cola de prioridad
    if cliente in hora_llegada_dict:
        # Si está en la cola, obtenemos su hora de llegada anterior
        hora_llegada_anterior = hora_llegada_dict[cliente]

        # Comparamos la hora de llegada anterior con la hora actual
        if hora_llegada_anterior > hora_actual:
            # Si el cliente llegó antes, actualizamos su hora de llegada en el diccionario
            hora_llegada_dict[cliente] = hora_actual
        else:
            # Si el cliente llegó después, no hacemos nada
            pass
    else:
        # Si el cliente no está en la cola, lo agregamos con su hora de llegada actual
        hora_llegada_dict[cliente] = hora_actual

    # Creamos una tupla con la prioridad y la hora de llegada para agregar a la cola de prioridad
    tupla_prioridad = (prioridad, hora_actual, cliente)

    # Agregamos la tupla a la cola de prioridad
    cola_prioridad.put(tupla_prioridad)

    return prioridad



def turnos(request):
    turnos = Turno.objects.order_by('prioridad', 'hora_llegada')
    return render(request, "turnos.html", {'turnos': turnos})

###########login####logout################
@csrf_protect
def inicio_sesion(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            # Redirige a la página deseada después del inicio de sesión exitoso
            return redirect('inicio')
        else:
            # El usuario no está autenticado, puedes mostrar un mensaje de error
            return render(request, 'login.html', {'error': 'Credenciales inválidas'})
    else:
        return render(request, 'login.html')
def cerrar_sesion(request):
    logout(request)
    # Redirige a la página deseada después del cierre de sesión exitoso
    return redirect('inicio')
###########login####logout################

#obtener los datos del servidor para autorellenar el formulario
def obtener_datos_cliente(request):

    if request.method == 'POST':
        ci = request.POST.get('ci')
        try:
            cliente = Cliente.objects.get(ci=ci)
            # Obtén los datos del cliente que deseas devolver
            data = {
                'name': cliente.name,
                'clientType': cliente.client_type
            }
            return JsonResponse(data)
        except Cliente.DoesNotExist:
            # Si no se encuentra el cliente, puedes devolver un mensaje de error
            data = {
                'error': 'Cliente no encontrado'
            }
            return JsonResponse(data, status=404)       
#obtener los datos del servidor para autorellenar el formulario